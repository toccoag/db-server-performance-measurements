# DB Server Performance Measurements

## See Also

* [Summary](SUMMARY.md)

## Servers

## db1.tocco.cust.vshn.net: Database Server at Cloudscale

Memory: 128 GiB (FLEX-128)

postgresql.conf:

```
(master-prod-database)peter.gerber@db1:~/dump$ grep -vP '^\s*(#|$)' /etc/postgresql/9.5/main/postgresql.conf | sort
archive_command = 'rsync -aog --ignore-existing --perms --chown=replication:postgres --chmod=770 %p replication@db2.tocco.cust.vshn.net:/var/lib/postgresql/wal_archive/%f'
archive_mode = on
cluster_name = '9.5/main' # added to process titles if nonempty
data_directory = '/var/lib/postgresql/9.5/main' # use data in another directory
datestyle = 'iso, mdy'
default_text_search_config = 'pg_catalog.english'
dynamic_shared_memory_type = posix # the default is the first option
effective_cache_size = 96GB
external_pid_file = '/var/run/postgresql/9.5-main.pid' # write an extra PID file
hba_file = '/etc/postgresql/9.5/main/pg_hba.conf' # host-based authentication file
ident_file = '/etc/postgresql/9.5/main/pg_ident.conf' # ident configuration file
lc_messages = 'en_US.UTF-8' # locale for system error message
lc_monetary = 'en_US.UTF-8' # locale for monetary formatting
lc_numeric = 'en_US.UTF-8' # locale for number formatting
lc_time = 'en_US.UTF-8' # locale for time formatting
listen_addresses = '*'
log_line_prefix = '%t ' # special values:
log_min_duration_statement = 180000
log_timezone = localtime
maintenance_work_mem = 512MB
max_connections = 2000 # (change requires restart)
max_locks_per_transaction = 256 # min 10
max_wal_senders = 2
port = 5432 # (change requires restart)
shared_buffers = 32GB # min 128kB
ssl_cert_file = '/etc/ssl/certs/db1.tocco.cust.vshn.net-chained.pem' # (change requires restart)
ssl_ciphers = 'TLSv1.2:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK'
ssl_key_file = '/etc/ssl/private/db1.tocco.cust.vshn.net-key.pem' # (change requires restart)
ssl_prefer_server_ciphers = true
ssl = true # (change requires restart)
statement_timeout = 1h
stats_temp_directory = '/var/run/postgresql/9.5-main.pg_stat_tmp'
synchronous_commit = off # synchronization level;
timezone = localtime
unix_socket_directories = '/var/run/postgresql' # comma-separated list of directories
wal_buffers = 16MB
wal_keep_segments = 128
wal_level = hot_standby
work_mem = 8MB
```


## test-db1.tocco.cust.vshn.net: Test system at Cloudscale, DB in Ramdisk

Memory: 240 GiB (~130 GiB excluding ramdisk) (FLEX-240)

postgresql.conf (config from Puppet except that archiving is disabled):

```
(database-prod-test)peter.gerber@db-test1:~$ grep -vP '^\s*(#|$)' /etc/postgresql/9.5/main/postgresql.conf | sort
archive_command = 'true'
archive_mode = on               # enables archiving; off, on, or always
data_directory = '/var/lib/postgresql/9.5/main' # use data in another directory
datestyle = 'iso, mdy'
default_text_search_config = 'pg_catalog.english'
dynamic_shared_memory_type = posix # the default is the first option
effective_cache_size = 96GB
external_pid_file = '/var/run/postgresql/9.5-main.pid' # write an extra PID file
fsync = on # turns forced synchronization on or off
hba_file = '/etc/postgresql/9.5/main/pg_hba.conf' # host-based authentication file
ident_file = '/etc/postgresql/9.5/main/pg_ident.conf' # ident configuration file
lc_messages = 'en_US.UTF-8' # locale for system error message
lc_monetary = 'en_US.UTF-8' # locale for monetary formatting
lc_numeric = 'en_US.UTF-8' # locale for number formatting
lc_time = 'en_US.UTF-8' # locale for time formatting
listen_addresses = '*'
log_line_prefix = '%t ' # special values:
log_min_duration_statement = 180000
log_timezone = localtime
maintenance_work_mem = 512MB
max_connections = 2000 # (change requires restart)
max_locks_per_transaction = 256
max_wal_senders = 2
port = 5432 # (change requires restart)
shared_buffers = 32GB # min 128kB
ssl_cert_file = '/etc/ssl/certs/db-test1.tocco.cust.vshn.net-chained.pem' # (change requires restart)
ssl_ciphers = 'TLSv1.2:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK'
ssl_key_file = '/etc/ssl/private/db-test1.tocco.cust.vshn.net-key.pem' # (change requires restart)
ssl_prefer_server_ciphers = true
ssl = true # (change requires restart)
statement_timeout = 0 # in milliseconds, 0 is disabled
statement_timeout = 1h
stats_temp_directory = '/var/run/postgresql/9.5-main.pg_stat_tmp'
synchronous_commit = on # synchronization level;
timezone = localtime
unix_socket_directories = '/var/run/postgresql' # comma-separated list of directories
wal_buffers = 16MB
wal_keep_segments = 128
wal_level = hot_standby
work_mem = 64MB
```

## db01master.tocco.ch: Legacy DB Server at Nine

CPU: 2x Intel(R) Xeon(R) CPU E5-2640 v4 @ 2.40GHz

Memory: 256 GiB

postgresql.conf:

```
tadm@db01c:/etc/postgresql/9.5/main$ grep -vP '^\s*(#|$)' /etc/postgresql/9.5/main/postgresql.conf | sort
archive_command = 'cp %p /postgres/wal_archive/%f'         # command to use to archive a logfile segment
archive_mode = on               # enables archiving; off, on, or always
autovacuum = on         # Enable autovacuum subprocess?  'on'
data_directory = '/postgres/postgres_data'              # use data in another directory
datestyle = 'iso, mdy'
default_text_search_config = 'pg_catalog.english'
dynamic_shared_memory_type = posix      # the default is the first option
effective_cache_size = 30GB
external_pid_file = '/var/run/postgresql/9.5-main.pid'                  # write an extra PID file
full_page_writes = on           # recover from partial page writes
hba_file = '/etc/postgresql/9.5/main/pg_hba.conf'       # host-based authentication file
ident_file = '/etc/postgresql/9.5/main/pg_ident.conf'   # ident configuration file
lc_messages = 'en_US.UTF-8'                     # locale for system error message
lc_monetary = 'en_US.UTF-8'                     # locale for monetary formatting
lc_numeric = 'en_US.UTF-8'                      # locale for number formatting
lc_time = 'en_US.UTF-8'                         # locale for time formatting
listen_addresses = '*'          # what IP address(es) to listen on;
log_line_prefix = '%t [%p-%l] %q%u@%d '                 # special values:
log_min_duration_statement = 180000     # -1 is disabled, 0 logs all statements
log_timezone = 'localtime'
maintenance_work_mem = 564MB            # min 1MB
max_connections = 2000                  # (change requires restart)
max_locks_per_transaction = 256 # min 10
max_replication_slots = 2       # max number of replication slots
max_wal_senders = 4             # max number of walsender processes
port = 5432                             # (change requires restart)
shared_buffers = 22GB # min 128kB
shared_preload_libraries = 'auto_explain'
ssl_cert_file = '/etc/ssl/certs/ssl-cert-snakeoil.pem'          # (change requires restart)
ssl_key_file = '/etc/ssl/private/ssl-cert-snakeoil.key'         # (change requires restart)
ssl = true                              # (change requires restart)
statement_timeout = 86400000                    # in milliseconds, 0 is disabled
stats_temp_directory = '/var/run/postgresql/9.5-main.pg_stat_tmp'
synchronous_commit = local # synchronization level;
temp_buffers = 18MB                     # min 800kB
timezone = 'localtime'
unix_socket_directories = '/var/run/postgresql' # comma-separated list of directories
wal_keep_segments = 100         # in logfile segments, 16MB each; 0 disables
wal_level = hot_standby # minimal, archive, hot_standby, or logical
wal_log_hints = on                      # also do full page writes of non-critical updates
work_mem = 14MB                         # min 64kB
```

## Test: perf1 ##

This test involves this query only:

```sql
explain analyze select distinct user0_."pk" as pk1_736_, user0_."class" as class2_736_, user0_."abbreviation" as abbrevia3_736_, user0_."account_nr" as account_4_736_, user0_."ahv_nr" as ahv_nr5_736_, user0_."apprenticeship_end_date" as apprenti6_736_, user0_."apprenticeship_start_date" as apprenti7_736_, user0_."b_address" as b_addres8_736_, user0_."bank_account" as bank_acc9_736_, user0_."bank_sort_code" as bank_so10_736_, user0_."best_reached" as best_re11_736_, user0_."birthdate" as birthda12_736_, user0_."birthdate_sorting" as birthda13_736_, user0_."bms" as bms14_736_, user0_."c_address" as c_addre15_736_, user0_."callname" as callnam16_736_, user0_."children" as childre17_736_, user0_."comment" as comment18_736_, user0_."contract_nr" as contrac19_736_, user0_."_nice_create_timestamp" as _nice_c20_736_, user0_."_nice_create_user" as _nice_c21_736_, user0_."detail_picture" as detail_22_736_, user0_."email" as email23_736_, user0_."email_alternative" as email_a24_736_, user0_."fax" as fax25_736_, user0_."firstname" as firstna26_736_, user0_."hometown" as hometow27_736_, user0_."hometown2" as hometow28_736_, user0_."iban_nr" as iban_nr29_736_, user0_."instructor_id" as instruc30_736_, user0_."job" as job31_736_, user0_."lastname" as lastnam32_736_, user0_."learner_nr" as learner33_736_, user0_."other_language" as other_l34_736_, user0_."other_title" as other_t35_736_, user0_."personnel_number" as personn36_736_, user0_."phone_company" as phone_c37_736_, user0_."phone_mobile" as phone_m38_736_, user0_."phone_private" as phone_p39_736_, user0_."preview_picture" as preview40_736_, user0_."profession" as profess41_736_, user0_."publish" as publish42_736_, user0_."publish_detail" as publish43_736_, user0_."fk_academic_title" as fk_acad53_736_, user0_."fk_address_c" as fk_addr54_736_, user0_."fk_apprenticeship_current_year" as fk_appr55_736_, user0_."fk_bms_type" as fk_bms_56_736_, user0_."fk_civil_status" as fk_civi57_736_, user0_."fk_consultant" as fk_cons58_736_, user0_."fk_correspondence_language" as fk_corr59_736_, user0_."fk_employment" as fk_empl60_736_, user0_."fk_gender" as fk_gend61_736_, user0_."fk_job_variant" as fk_job_62_736_, user0_."fk_learner_status" as fk_lear63_736_, user0_."fk_meals" as fk_meal64_736_, user0_."fk_nationality" as fk_nati65_736_, user0_."fk_native_language" as fk_nati66_736_, user0_."fk_profile" as fk_prof67_736_, user0_."fk_salutation" as fk_salu68_736_, user0_."fk_subvention_canton" as fk_subv69_736_, user0_."fk_uek_contract_type" as fk_uek_70_736_, user0_."fk_user_business" as fk_user71_736_, user0_."fk_user_residence_status" as fk_user72_736_, user0_."fk_user_source" as fk_user73_736_, user0_."fk_user_status" as fk_user74_736_, user0_."social_security_nr" as social_44_736_, user0_."spouse" as spouse45_736_, user0_."_nice_update_timestamp" as _nice_u46_736_, user0_."_nice_update_user" as _nice_u47_736_, user0_."user_nr" as user_nr48_736_, user0_."user_nr_old" as user_nr49_736_, user0_."uuid" as uuid50_736_, user0_."_nice_version" as _nice_v51_736_, user0_."website" as website52_736_ from "nice_user" user0_ left outer join "nice_user_status" user_statu1_ on user0_."fk_user_status"=user_statu1_."pk" where user_statu1_."unique_id"<>'archive' and (exists (select relregistr2_."pk" from "nice_registration" relregistr2_ left outer join "nice_event" event3_ on relregistr2_."fk_event"=event3_."pk" left outer join "nice_business_unit" business_u4_ on event3_."fk_business_unit"=business_u4_."pk" left outer join "nice_event_status" event_stat5_ on event3_."fk_event_status"=event_stat5_."pk" left outer join "nice_event_type" event_type6_ on event3_."fk_event_type"=event_type6_."pk" left outer join "nice_registration_status" registrati7_ on relregistr2_."fk_registration_status"=registrati7_."pk" where user0_."pk"=relregistr2_."fk_user" and 1=1 and business_u4_."unique_id"='ibz' and event_stat5_."unique_id"='open' and (event_type6_."unique_id" in ('training' , 'education' , 'internal_event')) and (registrati7_."unique_id" in ('registered' , 'booked')) and (exists (select event8_."pk" from "nice_event" event8_ where relregistr2_."fk_event"=event8_."pk" and (exists (select relreserva9_."pk" from "nice_reservation" relreserva9_ where event8_."pk"=relreserva9_."fk_event" and 1=1 and relreserva9_."date_till">'05-Jun-18'))))) or exists (select relregistr10_."pk" from "nice_registration" relregistr10_ left outer join "nice_event" event11_ on relregistr10_."fk_event"=event11_."pk" left outer join "nice_business_unit" business_u12_ on event11_."fk_business_unit"=business_u12_."pk" left outer join "nice_seminar" seminar13_ on event11_."fk_seminar"=seminar13_."pk" left outer join "nice_seminar_is_template" seminar_is14_ on seminar13_."fk_seminar_is_template"=seminar_is14_."pk" left outer join "nice_event_type" event_type15_ on event11_."fk_event_type"=event_type15_."pk" where user0_."pk"=relregistr10_."fk_user" and 1=1 and business_u12_."unique_id"='ibz' and (seminar_is14_."unique_id" in ('planned' , 'active')) and event_type15_."sorting"<200 and (exists (select event16_."pk" from "nice_event" event16_ where relregistr10_."fk_event"=event16_."pk" and (exists (select relreserva17_."pk" from "nice_reservation" relreserva17_ where event16_."pk"=relreserva17_."fk_event" and 1=1 and relreserva17_."date_till">=(CAST('today' AS TIMESTAMP) + CAST('-3 month' AS INTERVAL))))) or exists (select event18_."pk" from "nice_event" event18_ where relregistr10_."fk_event"=event18_."pk" and (exists (select event20_."pk" from "nice_parallel_original_to_parallel_event" relparalle19_, "nice_event" event20_ where event18_."pk"=relparalle19_."fk_parallel_original" and relparalle19_."fk_parallel_event"=event20_."pk" and (exists (select relreserva21_."pk" from "nice_reservation" relreserva21_ where event20_."pk"=relreserva21_."fk_event" and 1=1 and relreserva21_."date_till">=(CAST('today' AS TIMESTAMP) + CAST('-3 month' AS INTERVAL)))))))))) order by user0_."lastname" asc nulls last, user0_."firstname" asc nulls last;
```

Data database has been reduced to the tables involved in the query only, size 362 MiB.

Dump is located at db1.tocco.cust.vshn.net:/home/peter.gerber/dump/perf1.psql (run ANALYZE after restore to get meaningful results).


### Results

I measured 5 times on all machines.

db1.tocco.cust.vshn.net:

1. planning 439.592 ms + execution 9992.706 ms (no cache)
2. planning 30.546 ms + execution 1113.977 ms
3. planning 37.088 ms + execution 1351.567 ms
4. planning 50.326 ms + execution 1472.721 ms
5. planning 33.482 ms + execution 1223.681 ms

db-test1.tocco.cust.vshn.net (ramdisk):

1. planning 72.320 + 1307.558 ms (no cache)
2. planning 33.033 ms + 967.462 ms
3. planning 34.049 ms + 1019.308 ms
4. planning 32.482 ms + 1269.587 ms
5. planning 34.071 ms + 974.319 ms

db2.tocco.cust.vshn.net:

1.  451.479 ms + 11612.212 ms (inode cache emptied, Postgres restarted)
2.   26.691 ms + 1344.498 ms

--

1.  308.380 ms + 9542.643 ms (inode cache emptied, Postgres restarted)
2.   39.976 ms + 1227.644 ms

db-test1.tocco.cust.vshn.net (SSD):

(Copied setting from db1 for this test and disable archiving.)

1.  377.986 ms + 11093.551 ms (inode cache emptied, Postgres restarted)
2.   33.233 ms + 970.008 ms

--

1. 407.416 ms + 11981.389 ms (inode cache emptied, Postgres restarted)
2. 35.011 ms + 954.277 ms

db-test1.tocco.cust.vshn.net (NVMe):

(Copied setting from db1 for this test and disable archiving.)

1.  338.513 ms + 8031.865 ms (inode cache emptied, Postgres restarted)
2.   38.930 ms + 992.297 ms

--

1.  395.059 ms + 12980.528 ms (inode cache emptied, Postgres restarted)
2.  60.930 ms + 986.412 ms

--

1.  416.102 ms + 12559.266 ms (inode cache emptied, Postgres restarted)
2.  35.064 ms + 984.665 ms

db01master.tocco.ch:

1. planning 99.952 ms + 1970.609 ms (no cache)
2. planning 30.551 ms + 974.114 ms
3. planning 30.502 ms + 1102.842 ms
4. planning 30.560 ms + 1080.753 ms
5. planning 27.184 ms + 1190.249 ms

db-test2-exo.tocco.cust.vshn.net (Exoscale):

1. planning 164.735 ms + execution 3745.402 ms (inode cache emptied, Postgres restarted)
2. planning time: 26.409 ms + execution 749.701 ms

--

1. planning 134.825 ms + execution 3570.488 ms (inode cache emptied, Postgres restarted)
2. planning 17.848 ms + execution 747.303 ms


## Test: perf2

This test executes 41549 read-only queries. They've been recorded over several minutes while executing some DB-heavy actions in our Application.

Dump is located at db1.tocco.cust.vshn.net:/home/peter.gerber/dump/perf2.psql (1606 MiB) (run ANALYZE after restore to get meaningful results). The queries that were executed can be found at  db1.tocco.cust.vshn.net:/home/peter.gerber/dump/perf2_test.sql.

The time was measured using `time sudo -u postgres psql perf2 -q -f perf2_test.sql >/dev/null`.

### Results

db1.tocco.cust.vshn.net:


1.

    real    1m26.066s    # no cache
    user    0m2.208s
    sys     0m0.880s

2.

    real    0m50.443s
    user    0m2.256s
    sys     0m0.988s

3.

    real    0m46.505s
    user    0m2.008s
    sys     0m0.948s


db-test1.tocco.cust.vshn.net (ramdisk):

1.

    real    0m35.379s
    user    0m1.892s
    sys     0m0.864s

2.

    real    0m31.927s
    user    0m2.032s
    sys     0m1.028s

3.

    real    0m34.389s
    user    0m1.652s
    sys     0m0.584s


db2.tocco.cust.vshn.net:


1.

    real    1m14.509s  # inode cache emptied, Postgres restarted
    user    0m1.848s
    sys     0m1.004s

2.

    real    0m36.698s
    user    0m1.724s
    sys     0m0.912s


db-test1.tocco.cust.vshn.net (SSD):

1.

    real    1m6.731s  # inode cache emptied, Postgres restarted
    user    0m2.424s
    sys     0m1.140s

2.

    real    0m30.395s
    user    0m1.728s
    sys     0m0.756s

db-test1.tocco.cust.vshn.net (NVMe):

1.

    real    1m13.620s  # inode cache emptied, Postgres restarted
    user    0m2.724s
    sys     0m1.152s

2.

    real    0m30.319s
    user    0m1.672s
    sys     0m0.812s

1.

    real    1m5.948s  # inode cache emptied, Postgres restarted
    user    0m2.252s
    sys     0m0.896s

db01master.tocco.ch:

1.

    real    0m42.464s    # no cache
    user    0m1.488s
    sys     0m0.844s

2.

    real    0m34.628s
    user    0m1.556s
    sys     0m0.692s

3.

    real    0m35.090s
    user    0m1.452s
    sys     0m0.636s


db-test2-exo.tocco.cust.vshn.net (Exoscale):

1.

    real    0m35.483s  # inode cache emptied, Postgres restarted
    user    0m2.024s
    sys     0m1.208s

2.

    real    0m27.060s
    user    0m1.920s
    sys     0m1.128s

1.

    real    0m34.021s  # inode cache emptied, Postgres restarted
    user    0m1.920s
    sys     0m1.236s

2.

    real    0m26.681s
    user    0m2.020s
    sys     0m1.024s


## JMeter Tests: Series 1

Measurements were made using a JMeter instance running within Tocco's office network.

| Application                                | DB Server                                                | App Version | Report                                                     |
|--------------------------------------------|----------------------------------------------------------|-------------|------------------------------------------------------------|
| OpenShift Node (CloudScale)                | db1.tocco.cust.vshn.net (Cloudscale)                     | 2.12        | [2018-07-02][2018-07-02--openshift--db1]                   |
| Native on app server (Nine)                | db01master.tocco.ch (Nine)                               | 2.12        | [2018-07-02][2018-07-02--app03_native--db01master]         |
| OpenShift Node (CloudScale)                | test-db1.tocco.cust.vshn.net (Cloudscale, ramdisk)       | 2.12        | [2018-07-02][2018-07-02--openshift--test-db1_ramdisk]      |
| Docker container on app server (Nine)      | db01master.tocco.ch (Nine)                               | 2.12        | [2018-07-03][2018-07-02--docker_nine--db01master]          |
| OpenShift Node (CloudScale, segregated¹)   | db1.tocco.cust.vshn.net (Cloudscale)                     | 2.12        | [2018-08-22][2018-08-22--openshift-segregated--db1]        |
| OpenShift Node (CloudScale)                | db-test2.tocco.cust.vshn.net (Exoscale)                  | 2.12        | ~~[2018-08-24][2018-08-24--app_cs--db_exo]~~ ²             |

¹ One of the compute nodes was segrated to ensure only the application being tested was running on it.
² JMeter test involve some writing and fsync on Postgres was set to off by accident.

ODT file for printing is available [here](https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/print_series1.odt).

[2018-07-02--openshift--db1]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/db1.tocco.cust.vshn.net/index.html
[2018-07-02--app03_native--db01master]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/db01master.tocco.ch/index.html
[2018-07-02--openshift--test-db1_ramdisk]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/test-db1.tocco.cust.vshn.net/index.html
[2018-07-02--docker_nine--db01master]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/db01master_app03_docker/index.html
[2018-08-22--openshift-segregated--db1]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/db1.tocco.cust.vshn.net_segregated/index.html
[2018-08-24--app_cs--db_exo]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/app_cs--db_exo/index.html


Settings used for Docker-based tests:

```sh
docker run --rm -d --name perf \
-e NICE2_LOGBACK_CONFIG=logback_terminal \
-e NICE2_HIKARI_dataSource__databaseName=nice_perf \
-e NICE2_HIKARI_dataSource__password=XXXXXXXXXXXXXXXXXX \
-e NICE2_HIKARI_dataSource__serverName=172.17.1.11 \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__runenv=production \
-e NICE2_APP_hiveapp__StarterExecutor__starterTimeoutInMinutes=10 \
-e NICE2_APP_nice2__dbrefactoring__removeChangeLogLock=true \
-e NICE2_HIKARI_maximumPoolSize=5 \
-e NICE2_INSTALLATION=perf \
-e NICE2_APP_email__hostname=mxout1.tocco.cust.vshn.net \
-e NICE2_APP_recipientrewrite__enabled=true \
-e NICE2_APP_recipientrewrite__mappings="(.*)@tocco.ch -> $1@tocco.ch; (.*)@mail-tester.com -> $1@mail-tester.com; * -> test0@tocco.ch" \
-e NICE2_HIKARI_dataSource__sslMode=require \
-e NICE2_HIKARI_dataSource__user=nice_perf \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableLanguageSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableBusinessUnitSync=true \
-e MEMORY_LIMIT=10737418240 \
-e REQUESTED_MEMORY=6291456000 \
-v "$HOME/perf/var:/app/var" \
-p 12839:8080 \
registry.appuio.ch/toco-nice-bbg/nice
```

## JMeter Tests: Series 2

Measurements were made using a JMeter instance running directly on the application server.

| Application                                 | DB Server                                                | App Version | Report                                                       |
|---------------------------------------------|----------------------------------------------------------|-------------|--------------------------------------------------------------|
| Docker container on app server (Nine)       | db01master.tocco.ch (Nine)                               | 2.12        | [2018-07-24][2018-07-18--nine-nine]                          |
| Docker container on app server (Nine)       | db1.tocco.cust.vsnn.net (Cloudscale)                     | 2.12        | [2018-07-23][2018-07-23--nine-cs]                            |
| Docker container on app server (Cloudscale) | db01master.tocco.ch (Nine)                               | 2.12        | [2018-07-24][2018-07-23--cs-nine]                            |
| Docker container on app server (Cloudscale) | db1.tocco.cust.vsnn.net (Cloudscale)                     | 2.12        | [2018-07-23][2018-07-23--cs-cs]                              |
| Docker container on app server (Cloudscale) | db-test2.tocco.cust.vshn.net (Cloudscale, ramdisk)       | 2.12        | [2018-07-23][2018-08-21--cs-cs-ramdisk]                      |
| Docker container on app server (Exoscale)   | db-test2-exo.tocco.cust.vshn.net (Exoscale)              | 2.12        | ~~[2018-08-21][2018-08-21--exo-exo]~~ ¹                      |
| Docker container on app server (Exoscale)   | db1.tocco.cust.vsnn.net (Cloudscale)                     | 2.12        | [2018-08-29][2018-08-29--exo-cs]                             |
| Docker container on app server (Cloudscale) | db-test2-exo.tocco.cust.vsnn.net (Exoscale)              | 2.12        | ~~[2018-08-29][2018-08-29--cs-exo]~~ ¹                       |
| Docker container on app server (Nine)       | db01master.tocco.ch (Nine)                               | 2.17        | [2018-08-02][2018-08-02--nine-nine--v2.17]                   |
| Docker container on app server (Cloudscale) | db1.tocco.cust.vsnn.net (Cloudscale)                     | 2.17        | [2018-08-02][2018-08-02--cs-cs-v2.17]                        |

¹ JMeter test involve some writing and fsync on Postgres was set to off by accident.

ODT file for printing is available [here](https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/print_series2.odt).

[2018-07-23--cs-nine]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1.tocco.cust.vshn.net--db01master.tocco.ch/index.html
[2018-07-23--cs-cs]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1.tocco.cust.vshn.net--db1.tocco.cust.vshn.net/index.html
[2018-07-18--nine-nine]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app03.tocco.ch--db01master.tocco.ch/index.html
[2018-07-23--nine-cs]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app03.tocco.ch--db1.tocco.cust.vshn.net/index.html
[2018-08-21--cs-cs-ramdisk]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1.tocco.cust.vshn.net--db-test2-tocco.cust.vshn.net/index.html
[2018-08-21--exo-exo]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1-exo.tocco.cust.vshn.net--db-test2-exo.tocco.cust.vshn.net/index.html
[2018-08-02--nine-nine--v2.17]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app03.tocco.ch--db01master.tocco.ch--v2.17/index.html
[2018-08-02--cs-cs-v2.17]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1.tocco.cust.vshn.net--db1.tocco.cust.vshn.net--v2.17/index.html
[2018-08-29--exo-cs]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1-exo.tocco.cust.vshn.net--db1.tocco.cust.vshn.net/index.html
[2018-08-29--cs-exo]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series2/app-test1.tocco.cust.vshn.net--db-test2-exo.tocco.cust.vshn.net/index.html

### Docker Parameters

app-test1.tocco.cust.vshn.net with db1.tocco.cust.vshn.net as DB server

```
docker run -d --rm --name perf \
-e NICE2_LOGBACK_CONFIG=logback_terminal \
-e NICE2_HIKARI_dataSource__databaseName=nice_bbg_reduced_pege \
-e NICE2_HIKARI_dataSource__password=XXXXXXXXXX \
-e NICE2_HIKARI_dataSource__serverName=db1.tocco.cust.vshn.net \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__runenv=development \
-e NICE2_APP_hiveapp__StarterExecutor__starterTimeoutInMinutes=10 \
-e NICE2_APP_nice2__dbrefactoring__removeChangeLogLock=true \
-e NICE2_HIKARI_maximumPoolSize=5 \
-e NICE2_INSTALLATION=perf \
-e NICE2_APP_email__hostname=mxout1.tocco.cust.vshn.net.invalid \
-e NICE2_APP_recipientrewrite__enabled=true \
-e NICE2_APP_recipientrewrite__mappings="(.*)@tocco.ch -> $1@tocco.ch; (.*)@mail-tester.com -> $1@mail-tester.com; * -> test0@tocco.ch" \
-e NICE2_HIKARI_dataSource__sslMode=require \
-e NICE2_HIKARI_dataSource__user=nice_perf \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableLanguageSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableBusinessUnitSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__enterprisesearch__disableStartup=true \
-e MEMORY_LIMIT=3355443200 \
-e REQUESTED_MEMORY=3355443200 \
-v "$HOME/perf/var:/app/var" \
-p 8080:8080 \
--network host \
registry.appuio.ch/toco-nice-bbg/nice
```

app-test1.tocco.cust.vshn.net with db01master.tocco.ch as DB server:

```
docker run -d --rm --name perf \
-e NICE2_LOGBACK_CONFIG=logback_terminal \
-e NICE2_HIKARI_dataSource__databaseName=nice_bbg_reduced_pege \
-e NICE2_HIKARI_dataSource__password=XXXXXXXXXX \
-e NICE2_HIKARI_dataSource__serverName=db01master.tocco.ch \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__runenv=development \
-e NICE2_APP_hiveapp__StarterExecutor__starterTimeoutInMinutes=10 \
-e NICE2_APP_nice2__dbrefactoring__removeChangeLogLock=true \
-e NICE2_HIKARI_maximumPoolSize=5 \
-e NICE2_INSTALLATION=perf \
-e NICE2_APP_email__hostname=mxout1.tocco.cust.vshn.net.invalid \
-e NICE2_APP_recipientrewrite__enabled=true \
-e NICE2_APP_recipientrewrite__mappings="(.*)@tocco.ch -> $1@tocco.ch; (.*)@mail-tester.com -> $1@mail-tester.com; * -> test0@tocco.ch" \
-e NICE2_HIKARI_dataSource__sslMode=require \
-e NICE2_HIKARI_dataSource__user=nice_perf \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableLanguageSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableBusinessUnitSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__enterprisesearch__disableStartup=true \
-e MEMORY_LIMIT=10737418240 \
-e REQUESTED_MEMORY=3355443200 \
-v "$HOME/perf/var:/app/var" \
-p 8080:8080 \
--network host \
registry.appuio.ch/toco-nice-bbg/nice
```

app03.tocco.ch with db01master.tocco.ch as DB server:

```
docker run -d --rm --name perf \
-e NICE2_LOGBACK_CONFIG=logback_terminal \
-e NICE2_HIKARI_dataSource__databaseName=nice_bbg_reduced_pege \
-e NICE2_HIKARI_dataSource__password=XXXXXXXXXX \
-e NICE2_HIKARI_dataSource__serverName=172.17.1.11 \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__runenv=development \
-e NICE2_APP_hiveapp__StarterExecutor__starterTimeoutInMinutes=10 \
-e NICE2_APP_nice2__dbrefactoring__removeChangeLogLock=true \
-e NICE2_HIKARI_maximumPoolSize=5 \
-e NICE2_INSTALLATION=perf \
-e NICE2_APP_email__hostname=mxout1.tocco.cust.vshn.net \
-e NICE2_APP_recipientrewrite__enabled=true \
-e NICE2_APP_recipientrewrite__mappings="(.*)@tocco.ch -> $1@tocco.ch; (.*)@mail-tester.com -> $1@mail-tester.com; * -> test0@tocco.ch" \
-e NICE2_HIKARI_dataSource__sslMode=require \
-e NICE2_HIKARI_dataSource__user=nice_perf \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableLanguageSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableBusinessUnitSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__enterprisesearch__disableStartup=true \
-e MEMORY_LIMIT=3355443200 \
-e REQUESTED_MEMORY=3355443200 \
-v "$HOME/perf/var:/app/var" \
-p 8080:8080 \
registry.appuio.ch/toco-nice-bbg/nice
```

app03.tocco.ch (perf-nine.tocco.ch) with db1.tocco.cust.vshn.net as DB server:

```
docker run -d --rm --name perf \
-e NICE2_LOGBACK_CONFIG=logback_terminal \
-e NICE2_HIKARI_dataSource__databaseName=nice_bbg_reduced_pege \
-e NICE2_HIKARI_dataSource__password=XXXXXXXXXX \
-e NICE2_HIKARI_dataSource__serverName=db1.tocco.cust.vshn.net \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__runenv=development \
-e NICE2_APP_hiveapp__StarterExecutor__starterTimeoutInMinutes=10 \
-e NICE2_APP_nice2__dbrefactoring__removeChangeLogLock=true \
-e NICE2_HIKARI_maximumPoolSize=5 \
-e NICE2_INSTALLATION=perf \
-e NICE2_APP_email__hostname=mxout1.tocco.cust.vshn.net \
-e NICE2_APP_recipientrewrite__enabled=true \
-e NICE2_APP_recipientrewrite__mappings="(.*)@tocco.ch -> $1@tocco.ch; (.*)@mail-tester.com -> $1@mail-tester.com; * -> test0@tocco.ch" \
-e NICE2_HIKARI_dataSource__sslMode=require \
-e NICE2_HIKARI_dataSource__user=nice_perf \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableLanguageSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__disableBusinessUnitSync=true \
-e NICE2_JAVA_OPT____Dch__tocco__nice2__enterprisesearch__disableStartup=true \
-e MEMORY_LIMIT=3355443200 \
-e REQUESTED_MEMORY=3355443200 \
-v "$HOME/perf/var:/app/var" \
-p 8080:8080 \
registry.appuio.ch/toco-nice-bbg/nice
```
## JMeter Tests: Series 3

Jmeter tests with emptied cache comparing performance of Docker VS OpenShift.

| Application                                               | DB Server                                                   | App Version | Report                                                       |
|-----------------------------------------------------------|-------------------------------------------------------------|-------------|--------------------------------------------------------------|
| OpenShift (Cloud Scale)                                   | db-test2.tocco.cust.vshn.net (Cloudscale, SSD (no ramdisk)) | 2.12        | [2018-09-15 12:04 - 2018-09-15 22:57][openshift-cs]          |
| Docker on app-test1.tocco.cust.vshn.net (Cloud Scale)     | db-test2.tocco.cust.vshn.net (Cloudscale, SSD (no ramdisk)) | 2.12        | [2018-09-13 12:29 - 2018-09-13 20:41][docker-cs]             |
| Docker on app-test1-exo.tocco.cust.vshn.net (Exoscale)    | db-test2-exo.tocco.cust.vshn.net (Exoscale, SSD)            | 2.12        | [2018-09-14 08:13 - 2018-09-14 14:03][docker-exo]            |

[openshift-cs]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series3/cloudscale_openshift/index.html
[docker-cs]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series3/cloudscale_docker/index.html
[docker-exo]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/series3/exoscale_docker/index.html

Steps:

1. Ensure ramdisk isn't mounted (db-test2.tocco.cust.vshn.net)
1. Empty cache
  * `systemctl stop postgresql`
  * `sync && echo 3 > /proc/sys/vm/drop_caches`
  * `systemctl start postgresql`
2. Start Tocco
   * wait for startup tasks to complete
3. Execute JMeter tests
4. Update this page
5. Report results to VSHN, ask to have I/O load graph added here

Config OpenShift:

[openshift_dc.yaml](jmeter/series3/openshift_dc.yaml)

Config Docker:

```
docker run -d --name perf \
  -e NICE2_LOGBACK_CONFIG=logback_terminal
  -e NICE2_HIKARI_dataSource__databaseName=${DB_NAME}
  -e NICE2_HIKARI_dataSource__password=${PASSWORD}
  -e NICE2_HIKARI_dataSource__serverName=db-test2.tocco.cust.vshn.net
  -e NICE2_JAVA_OPT____Dch__tocco__nice2__runenv=development
  -e NICE2_APP_hiveapp__StarterExecutor__starterTimeoutInMinutes=10
  -e NICE2_APP_nice2__dbrefactoring__removeChangeLogLock=true
  -e NICE2_HIKARI_maximumPoolSize=5
  -e NICE2_INSTALLATION=perf
  -e NICE2_APP_email__hostname=mxout1.tocco.cust.vshn.net.invalid
  -e NICE2_APP_recipientrewrite__enabled=true
  -e NICE2_APP_recipientrewrite__mappings="(.*)@tocco.ch -> $1@tocco.ch; (.*)@mail-tester.com -> $1@mail-tester.com; * -> test0@tocco.ch"
  -e NICE2_HIKARI_dataSource__sslMode=require
  -e NICE2_HIKARI_dataSource__user=nice_perf
  -e NICE2_JAVA_OPT____Dch__tocco__nice2__disableLanguageSync=true
  -e NICE2_JAVA_OPT____Dch__tocco__nice2__disableBusinessUnitSync=true
  -e NICE2_JAVA_OPT____Dch__tocco__nice2__enterprisesearch__disableStartup=true
  -e MEMORY_LIMIT=3355443200
  -e REQUESTED_MEMORY=3355443200
  -v "$HOME/perf/var:/app/var" --network host jm
```

## PSQL Measurements in Docker and on OpenShift

The measurements have been made using the same database and queries used in the *perf2* test further up.

In the tests *Docker* and *OpenShift*, I executed the tests via `psql` from within a Docker container. In case of *Native*,
I executed `psql` directly on the server.

Native (app: test-app1.tocco.cust.vshn.net, db: test-db2.tocco.cust.vshn.net):

    real  1m18.870s     # no cache
    user  0m2.328s
    sys   0m1.684s

    real  1m18.056s     # no cache
    user  0m2.396s
    sys   0m1.668s

    real    1m9.788s
    user    0m2.444s
    sys     0m1.500s

    real    1m9.238s
    user    0m2.268s
    sys     0m1.796s

    real    1m7.841s
    user    0m2.312s
    sys     0m1.652s


Docker (app: Docker on test-app1.tocco.cust.vshn.net, db: test-db2.tocco.cust.vshn.net):

    real  1m22.173s     # no cache
    user  0m2.484s
    sys 0m2.204s

    real  1m21.158s     # no cache
    user  0m2.352s
    sys   0m2.308s

    real  1m10.803s
    user  0m2.420s
    sys   0m2.228s

    real  1m12.761s
    user  0m2.512s
    sys   0m2.156s


OpenShift (app: OpenShift, db: test-db2.tocco.cust.vshn.net):

    real    1m54.871s  # no cache
    user    0m4.428s
    sys     0m4.411s

    real    1m50.805s  # no cache
    user    0m4.610s
    sys     0m4.490s

    real    1m38.983s
    user    0m4.283s
    sys     0m4.228s

    real    1m42.088s
    user    0m4.243s
    sys     0m4.211s
