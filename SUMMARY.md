# Summary

## Test on Hardware-Level

### Disk Bandwidth (`dd`)

* Disk reads are 7 times slower at Cloudscale (measuring bandwidth).

See [TOCO-119](https://control.vshn.net/tickets/TOCO-119).

## Tests on DB-Level

### Long-Running Query ([perf1](README.md#test-perf1)):

* With no cache, Nine infrastructure is 5 times faster.
* With cache, Nine infrastructure is about 10-20% faster.
* With cache, Cloudscale with ramdisk and Nine are about the same speed.
* Without cache, Cloudscale ramdisk is faster.

### Short Queries ([perf2](README.md#test-perf2)):

~10,000 queries recorded over a time period of a few minutes

* Nine is 2 times faster without cache.
* Nine is 40% faster with cache.
* With cache, Cloudscale with ramdisk is on pair with Nine.
* Without cache, Cloudscale with ramdisk, as expected, is faster.

## Test on Application-Level (JMeter)

### Test [bbg (CloudScale)][bbg-2018-05-30] vs [bbgschulung (Nine)][bbg-2018-05-30]

One of the earliest measurements was bbg vs bbgschulung. The former being a production system
running on OpenShift and the latter being a staging system and near copy of bbg running at Nine.

* Cloudscale is half as fast on average (with Nine being up to 3.5 times faster)

[bbg-2018-05-30]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/bbg/index.html
[bbgschulung-2018-05-30]: https://toccoag.gitlab.io/db-server-performance-measurements/jmeter/bbgschulung/index.html

### [JMeter series1](README.md#jmeter-tests-series-1)

JMeter test running Tocco on OpenShift and our app servers at Nine respectively.

* Cloudscale is half as fast on average (40% - 300% depending on test).
* Test results for Cloudscale with ramdisk do not significantly diverge from those without ramdisk.

### [JMeter series2](README.md#jmeter-tests-series-2)

JMeter tests running Tocco in Docker directly (no OpenShift).

* Cloudscale is about half as fast.
* Tocco v2.17 is slower than v2.12.
* Cloudscale is ~30% slower on average when running v2.17 (up to ~110%).
